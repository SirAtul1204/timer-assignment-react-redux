## To run the app locally -

- Clone the repo
- `yarn install`
- `yarn start`

<a href="https://timer-assignment-react-redux.netlify.app/" target="_blank">Click Here to see the deployed version</a>
