import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  toggleLapButton,
  toggleResetButton,
  toggleStartButton,
  toggleStopButton,
} from "../Actions/buttonAction";
import { stopTimer } from "../Actions/watchAction";

const StopButton = () => {
  const dispatch = useDispatch();
  const { isStopButtonDisabled } = useSelector((state) => state.button);
  const handleStop = () => {
    dispatch(stopTimer());
    dispatch(toggleStopButton());
    dispatch(toggleStartButton());
    dispatch(toggleResetButton());
    dispatch(toggleLapButton());
  };
  return (
    <button
      disabled={isStopButtonDisabled}
      onClick={handleStop}
      className="button stop"
    >
      Stop
    </button>
  );
};

export default StopButton;
