import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import {
  toggleLapButton,
  toggleResetButton,
  toggleStartButton,
  toggleStopButton,
} from "../Actions/buttonAction";
import { startTimer } from "../Actions/watchAction";

const StartButton = () => {
  const dispatch = useDispatch();
  const { isStartButtonDisabled } = useSelector((state) => state.button);

  const handleStart = () => {
    const timer = setInterval(() => {
      dispatch(startTimer(timer));
    }, 10);

    dispatch(toggleStartButton());
    dispatch(toggleStopButton());
    dispatch(toggleResetButton());
    dispatch(toggleLapButton());
  };
  return (
    <button
      disabled={isStartButtonDisabled}
      onClick={handleStart}
      className="button start"
    >
      Start
    </button>
  );
};

export default StartButton;
