import { useSelector } from "react-redux";

const LapsContainer = () => {
  const { laps } = useSelector((state) => state.watch);
  return (
    <div className="lab_container">
      <ol>
        {laps.map((lap, index) => {
          const minutes = lap[0];
          const seconds = lap[1];
          const centiseconds = lap[2];
          return (
            <li key={lap.join("-")}>
              Lap {index + 1} =&gt;{" "}
              {minutes >= 10 ? minutes : "0" + String(minutes)} :{" "}
              {seconds >= 10 ? seconds : "0" + String(seconds)} :{" "}
              {centiseconds >= 10 ? centiseconds : "0" + centiseconds}
            </li>
          );
        })}
      </ol>
    </div>
  );
};

export default LapsContainer;
