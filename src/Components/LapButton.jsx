import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { addLap } from "../Actions/watchAction";

const LapButton = () => {
  const { isLapButtonDisabled } = useSelector((state) => state.button);

  const dispatch = useDispatch();

  const handleLap = () => {
    dispatch(addLap());
  };

  return (
    <button
      onClick={handleLap}
      disabled={isLapButtonDisabled}
      className="button lap"
    >
      Lap
    </button>
  );
};

export default LapButton;
