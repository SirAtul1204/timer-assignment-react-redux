import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { resetTimer } from "../Actions/watchAction";

const ResetButton = () => {
  const { isResetButtonDisabled } = useSelector((state) => state.button);

  const dispatch = useDispatch();

  const handleReset = () => {
    dispatch(resetTimer());
  };

  return (
    <button
      onClick={handleReset}
      disabled={isResetButtonDisabled}
      className="button reset"
    >
      Reset
    </button>
  );
};

export default ResetButton;
