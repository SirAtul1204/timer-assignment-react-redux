import { useSelector } from "react-redux";

const Watch = () => {
  const { minutes, seconds, centiseconds } = useSelector(
    (state) => state.watch
  );
  return (
    <div className="watch">
      {minutes >= 10 ? minutes : "0" + String(minutes)} :{" "}
      {seconds >= 10 ? seconds : "0" + String(seconds)} :{" "}
      {centiseconds >= 10 ? centiseconds : "0" + String(centiseconds)}
    </div>
  );
};

export default Watch;
