import Watch from "./Components/Watch";
import StartButton from "./Components/StartButton";
import StopButton from "./Components/StopButton";
import ResetButton from "./Components/ResetButton";
import LapButton from "./Components/LapButton";
import LapsContainer from "./Components/LapsContainer";

const App = () => {
  return (
    <div className="major">
      <Watch />
      <div className="minor">
        <StartButton />
        <StopButton />
        <ResetButton />
        <LapButton />
      </div>
      <LapsContainer />
    </div>
  );
};

export default App;
