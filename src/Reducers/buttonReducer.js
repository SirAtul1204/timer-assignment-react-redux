const initialState = {
  isStartButtonDisabled: false,
  isStopButtonDisabled: true,
  isResetButtonDisabled: false,
  isLapButtonDisabled: true,
};

export const buttonReducer = (state = initialState, action) => {
  let newState = { ...state };
  switch (action.type) {
    case "TOGGLE_START_BUTTON":
      newState.isStartButtonDisabled = !state.isStartButtonDisabled;
      break;
    case "TOGGLE_STOP_BUTTON":
      newState.isStopButtonDisabled = !state.isStopButtonDisabled;
      break;
    case "TOGGLE_RESET_BUTTON":
      newState.isResetButtonDisabled = !state.isResetButtonDisabled;
      break;
    case "TOGGLE_LAP_BUTTON":
      newState.isLapButtonDisabled = !state.isLapButtonDisabled;
      break;
    default:
      break;
  }

  return newState;
};
