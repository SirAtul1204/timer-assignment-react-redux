import { watchReducer } from "./watchReducer";
import { buttonReducer } from "./buttonReducer";
import { combineReducers } from "redux";

export const rootReducer = combineReducers({
  watch: watchReducer,
  button: buttonReducer,
});
